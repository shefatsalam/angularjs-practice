'use strict';

eventsApp.controller('EventController', ['$scope', function($scope){
  //$scope.snippet='<span style="color:red">Hi there</sapn>';
  $scope.sortOrder='name';
  $scope.boolValue = true;
  $scope.myclass = "blue";
  $scope.event={
    name:'Angular Boot Camp',
    date:'1/30/2017',
    time:"10:30 am",
    location:{
      address:'Google Headquaters',
      city:'Mpuntaing View',
      province:'CA',
    },
    imageUrl:'https://angularjs.org/img/AngularJS-large.png',
    sessions:[
       {
        name:'Directive Masterclass Introductory',
        creatorName:'Bob Smith',
        duration:1,
        level:'Advanced',
        abstract:'In this sesison you will learn the ins and outs of directives!',
        upVoteCount:15,

      },
       {
        name:'Scopes for fun and profit intermediate',
        creatorName:'John Doe',
        duration:2,
        level:'Introductory',
        abstract:'This session will take a closer look at scopes. Learn what they do, how thy do it!',
        upVoteCount:0,

      },
      {
        name:'Well Behaved Controllers advanced',
        creatorName:'Jane Doe',
        duration:'2 hr',
        level:'Intermediate',
        abstract:'Controllers are the beginning of everything Angular does',
        upVoteCount:0,

      },
    ]
  }
  $scope.upVoteSession = function(session){
    session.upVoteCount++;
  }
  $scope.downVoteSession = function(session){
    session.upVoteCount--;
  }
}]);
