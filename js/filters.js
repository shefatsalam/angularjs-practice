'use strict';

/* Filters */

eventsApp.filter('durations', function(){
  return function(duration){
    switch(duration){
           case .5:return "Half Hours";
           case 1:return "1 Hours";
           case 2:return "Half Day";
           case 3:return "Full Day";
    }
  }
});
